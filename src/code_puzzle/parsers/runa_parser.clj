(ns code-puzzle.parsers.runa-parser
  (:require [code-puzzle.parsers.core :refer [parser]]))

(defn to-dollars [cents]
  "Given cents, convert to dollars"
  (/ (* cents 1.0) 100))

(defn mapentry-to-dollars [[k v]]
  "Given a map entry in cents, convert the header and the value to dollars"
  (let [matched (re-matches #"(.*)-cents" k)
        discountprovider (second matched)]
    (if matched
      {(str discountprovider "-dollars") (to-dollars v)}
      {k v}))) 

(defn lineitem-to-dollars [lineitem]
  "Given a lineitem.  convert all the columns to dollars if needed"
  (apply merge (map mapentry-to-dollars lineitem)))

(def runa-parser (fn []
                   (map lineitem-to-dollars  ((parser "runa_data.csv" \newline \,)))))
