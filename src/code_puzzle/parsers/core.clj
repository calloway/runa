(ns code-puzzle.parsers.core)

(defn lowercase 
  "Lower cases a word would have used clojure.string/lower-case - see caveats"
  [word]
  (.toLowerCase word)) 

(defn spaces-to-dashes 
  "Given words, convert the spaces to dashes.  Good for headers - see caveats"
  [words]
  (.replaceAll words " " "-"))

(defn split-on
  "Given a line with words and a delimiter.  Split the line on the delimiter"
  ([line delim] (split-on line delim [] []))
  ([line delim acc accline]
     (condp = (first line)
       nil (conj acc accline)
       delim (split-on (rest line) delim (conj acc accline) [])
       (split-on (rest line) delim acc (conj accline (first line))))))

(defn- to-double-or-not 
  "Converts the input to double if it is a double.  Else return input"
  [input]
  (try
    (Double/parseDouble input)
    (catch NumberFormatException e
      input)))

(defn- parse-line 
  "Given a line and delimter, convert the line to values"
  [line delim]
  (map #(apply str %) (split-on line delim)))

(defn normalize-spaces-and-case 
  "Given a header, lowercase and add dashes to normalize headers"
  [headers]
  (map (comp lowercase spaces-to-dashes) headers))

(defn normalize-mapentry 
  "Normalize mapentry, converting cents to dollars"
  [[k v]]
  (cond (or (re-matches #"(.*)-cents" k) (re-matches #"(.*)-dollars" k))
        {k (to-double-or-not v)}
        :else {k (lowercase v)}))

(defn record-to-numbers [record]
  (apply merge (map normalize-mapentry record)))

(defn convert-data-to-numbers [records]
  (map record-to-numbers records))


(defn parser 
  "Given a filename in resources, and record delimiter, column delimiter.  Returns a function that can parse the file into clojure data structure"

  [filename record-delim column-delim]
  (fn []
    (let [file (slurp (clojure.java.io/resource filename))
          lines (split-on (seq file) record-delim)
          headers (normalize-spaces-and-case (parse-line (first lines) column-delim))
          data (map #(parse-line % column-delim) (rest lines))]
      (convert-data-to-numbers (map #(zipmap headers %) data)))))
