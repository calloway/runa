(ns code-puzzle.report-service
  (:require [code-puzzle.summary-service :refer [summary-service]]
            [code-puzzle.order-service :refer [order-service]]
            [code-puzzle.parsers.merchant-parser :refer [merchant-parser]]
            [code-puzzle.parsers.runa-parser :refer [runa-parser]]))

(defn generate-report 
  "Generates report given a order-by and direction.  Report contains summary and runa and merchant orders.

Optional keyword arguments
:order-by - :order-id, :session-type :unit-price-dollars
:direction - :asc, :desc"

  [& {:keys [order-by direction] :or {order-by :order-id direction :asc}}]
  {:pre [(contains? #{:asc :desc} direction)
         (contains? #{:order-id :session-type :unit-price-dollars} order-by)]}
  (let [runa-raw-data (runa-parser)
        merchant-raw-data (merchant-parser)
        order-service1 (order-service runa-raw-data merchant-raw-data)
        summary-service1 (summary-service runa-raw-data merchant-raw-data)] 
    (->> (order-service1 :order-by order-by :direction direction)
         (hash-map "orders")
         (conj {"summaries" (summary-service1) }))))
