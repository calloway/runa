(ns code-puzzle.summary-service)

(defn- niladd 
  "Adds a and b.  If either is nil, treat nil as 0"
  [a b]
  (cond (nil? a) b
        (nil? b) a
        :else (+ a b)))

(defn- sum-column [column map1 map2]
  {column (niladd (map1 column) (map2 column))})

(defn- summarize 
  "Given 2 entries, sum the dollar fields"
  [map1 map2]
  (merge (sum-column "unit-price-dollars" map1 map2)
         (sum-column "merchant-discount-dollars" map1 map2)
         (sum-column "runa-discount-dollars" map1 map2)))

(defn get-summary 
  "Returns the summary on the dollar fields for given records"
  [records]
  (reduce summarize {} records))

(defn summary-service 
  "Summary service which returns summary info for runa orders and merchant orders"
  [runa-raw-data merchant-raw-data]
  (fn summaries []
    (conj (hash-map "runa-summary" (get-summary runa-raw-data))
          {"merchant-summary" (get-summary merchant-raw-data)})))
