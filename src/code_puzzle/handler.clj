(ns code-puzzle.handler
  (:use compojure.core)
  (:require [compojure.handler :as handler]
            [compojure.route :as route]
            [ring.middleware.json :refer [wrap-json-response]]
            [ring.util.response :refer [response]]
            [code-puzzle.report-service :refer [generate-report]]))

(defn get-order-by-param 
  "Parses the params to see what to order by.  defaults to by-id"
  [params]
  (if-let [matched (re-matches #"(.*)-(asc|desc)" (params "order_by"))]
    (keyword (second matched))
    :by-id))

(defn get-asc-or-desc-by-param 
  "Parses the params to see if asc or desc.  defaults to asc"
  [params]
  (if-let [matched (re-matches #"(.*)-(asc|desc)" (params "order_by"))]
    (keyword (nth matched 2))
    :asc))

(defroutes my-routes
  "Only json route /runatic/report when order-by param is provided."
  (GET "/runatic/report" {params :query-params} 
       (if-not (empty? params) 
         (response (generate-report :order-by (get-order-by-param params) 
                                    :direction (get-asc-or-desc-by-param params)))))
  (route/not-found "Page not found"))

(def app (wrap-json-response (handler/site my-routes)))
