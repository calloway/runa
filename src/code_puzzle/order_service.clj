(ns code-puzzle.order-service)


(defn- compare-asc [column]
  "Comparator.  compares by ascending"
  (fn [item1 item2]
    (> 0 (compare (item1 column) (item2 column)))))


(def by-order-id (compare-asc "order-id"))
(def by-session-type (compare-asc "session-type"))
(def by-unit-price-dollars (compare-asc "unit-price-dollars"))


(defn- order-with-comparator 
  "Orders records by direction.   Default direction is asc"

  [comparator records & {:keys [direction] :or {direction :asc} }]
  (sort (get {:asc comparator 
              :desc (complement comparator)} direction) 
        records))


(defn orders-by 
  "Orders by order-by and direction.  Defaults to ordery-by by-order-id and direction asc"

  [records & {:keys [order-by direction] :or {order-by :order-id direction :asc}}]  
  (order-with-comparator (get {:session-type by-session-type
                               :unit-price-dollars by-unit-price-dollars
                               :order-id by-order-id} order-by)
                         records
                         :direction direction))


(defn- get-related-merchant-order 
  "For each runa-data order find the corresponding merchant-data order matching on order-id"

  [data merchant-by-id]  
  (merchant-by-id ((data "runa-data") "order-id")))

(defn order-service 
  "Order Service which returns the combined runa-orders and merchant-orders"

  [runa-raw-data merchant-raw-data]  
  (fn combined-orders [& {:keys [order-by direction] :or {order-by :order-id direction :asc}}]
    (let [runa-orders (orders-by runa-raw-data :order-by order-by :direction direction)
          runa-orders-map (map (fn[s] (hash-map "runa-data" s)) runa-orders)
          merchant-orders (orders-by merchant-raw-data :order-by order-by :direction direction)
          merchant-orders-map (map (fn[s] (hash-map "merchant-data" s)) merchant-orders)
          merchant-orders-map-by-id (apply merge (map (fn[s] (hash-map (s "order-id") s)) merchant-orders))]
      (map (fn [s] (conj s (hash-map "merchant-data" (get-related-merchant-order s merchant-orders-map-by-id)))) runa-orders-map))))




