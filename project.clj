(defproject code-puzzle "0.1.0-SNAPSHOT"
  :description "Solution to Runa Code Puzzle"
  :plugins [[lein-ring "0.8.10"]]
  :dependencies [[compojure "1.1.6"]
                 [ring/ring-json "0.2.0"]
                 [org.clojure/data.json "0.2.4"]
                 [org.clojure/clojure "1.5.1"]]

  :ring {:handler code-puzzle.handler/app}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring-mock "0.1.5"]]}}
  )
