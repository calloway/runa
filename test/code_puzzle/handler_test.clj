(ns code-puzzle.handler-test
  (:require [clojure.test :refer :all]
            [ring.mock.request :refer :all]
            [clojure.data.json :as json]
            [code-puzzle.handler :refer :all]))


(defn mock-requests [f]
  (def response-by-session-type (app (request :get "/runatic/report?order_by=session-type-desc")))  
  (def response-by-order-id (app (request :get "/runatic/report?order_by=order-id-asc")))
  (def response-by-unit-price-dollars (app (request :get "/runatic/report?order_by=unit-price-dollars-asc")))
  (f)
)

(use-fixtures :once mock-requests)


(def expected-body-response-by-order-id (json/read-str "{ \"summaries\":{\"runa-summary\":{ \"unit-price-dollars\":360.5,
                                     \"merchant-discount-dollars\":15.0,
                                     \"runa-discount-dollars\":65.2}, 
                   \"merchant-summary\":{ \"unit-price-dollars\":360.0, 
                                          \"merchant-discount-dollars\":12.2,
                                          \"runa-discount-dollars\":65.3}},

   \"orders\":[{\"runa-data\":{\"unit-price-dollars\":110.5,
                                             \"merchant-discount-dollars\":10.0,
                                             \"runa-discount-dollars\":20.2,
                                             \"session-type\":\"control\",
                                             \"order-id\": \"72144305\"},
                              \"merchant-data\":{\"unit-price-dollars\":110.0,
                                                 \"merchant-discount-dollars\":10.2,
                                                 \"runa-discount-dollars\":20.5,
                                                 \"session-type\":\"control\",
                                                 \"order-id\": \"72144305\"}},
                {\"runa-data\":{\"unit-price-dollars\":200.0,
                                             \"merchant-discount-dollars\":0.0,
                                             \"runa-discount-dollars\":30.0,
                                             \"session-type\":\"test\",
                                             \"order-id\": \"72144777\"},
                              \"merchant-data\":{\"unit-price-dollars\":200.0,
                                                 \"merchant-discount-dollars\":0.0,
                                                 \"runa-discount-dollars\":30.0,
                                                 \"session-type\":\"test\",
                                                 \"order-id\": \"72144777\"}},
               {\"runa-data\":{\"unit-price-dollars\":50.0,
                                             \"merchant-discount-dollars\":5.0, 
                                             \"runa-discount-dollars\":15.0,
                                             \"session-type\":\"unmanaged\",
                                             \"order-id\": \"72145239\"}, 
                               \"merchant-data\":{\"unit-price-dollars\":50.0,
                                                  \"merchant-discount-dollars\":2.0, 
                                                  \"runa-discount-dollars\":14.8,
                                                  \"session-type\":\"unmanaged\",
                                                  \"order-id\": \"72145239\"}}]}}"))

(def expected-body-response-by-session-type
    (json/read-str "{ \"summaries\":{\"runa-summary\":{ \"unit-price-dollars\":360.5,
                                     \"merchant-discount-dollars\":15.0,
                                     \"runa-discount-dollars\":65.2}, 
                   \"merchant-summary\":{ \"unit-price-dollars\":360.0, 
                                          \"merchant-discount-dollars\":12.2,
                                          \"runa-discount-dollars\":65.3}},

   \"orders\":[{\"runa-data\":{\"unit-price-dollars\":50.0,
                                             \"merchant-discount-dollars\":5.0, 
                                             \"runa-discount-dollars\":15.0,
                                             \"session-type\":\"unmanaged\",
                                             \"order-id\": \"72145239\"}, 
                               \"merchant-data\":{\"unit-price-dollars\":50.0,
                                                  \"merchant-discount-dollars\":2.0, 
                                                  \"runa-discount-dollars\":14.8,
                                                  \"session-type\":\"unmanaged\",
                                                  \"order-id\": \"72145239\"}}, 

               {\"runa-data\":{\"unit-price-dollars\":200.0,
                                             \"merchant-discount-dollars\":0.0,
                                             \"runa-discount-dollars\":30.0,
                                             \"session-type\":\"test\",
                                             \"order-id\": \"72144777\"},
                              \"merchant-data\":{\"unit-price-dollars\":200.0,
                                                 \"merchant-discount-dollars\":0.0,
                                                 \"runa-discount-dollars\":30.0,
                                                 \"session-type\":\"test\",
                                                 \"order-id\": \"72144777\"}},

                {\"runa-data\":{\"unit-price-dollars\":110.5,
                                             \"merchant-discount-dollars\":10.0,
                                             \"runa-discount-dollars\":20.2,
                                             \"session-type\":\"control\",
                                             \"order-id\": \"72144305\"},
                              \"merchant-data\":{\"unit-price-dollars\":110.0,
                                                 \"merchant-discount-dollars\":10.2,
                                                 \"runa-discount-dollars\":20.5,
                                                 \"session-type\":\"control\",
                                                 \"order-id\": \"72144305\"}}]}}"))

(def expected-body-response-by-unit-price-dollars
  (json/read-str "{ \"summaries\":{\"runa-summary\":{ \"unit-price-dollars\":360.5,
                                     \"merchant-discount-dollars\":15.0,
                                     \"runa-discount-dollars\":65.2}, 
                   \"merchant-summary\":{ \"unit-price-dollars\":360.0, 
                                          \"merchant-discount-dollars\":12.2,
                                          \"runa-discount-dollars\":65.3}},

   \"orders\":[{\"runa-data\":{\"unit-price-dollars\":50.0,
                                             \"merchant-discount-dollars\":5.0, 
                                             \"runa-discount-dollars\":15.0,
                                             \"session-type\":\"unmanaged\",
                                             \"order-id\": \"72145239\"}, 
                               \"merchant-data\":{\"unit-price-dollars\":50.0,
                                                  \"merchant-discount-dollars\":2.0, 
                                                  \"runa-discount-dollars\":14.8,
                                                  \"session-type\":\"unmanaged\",
                                                  \"order-id\": \"72145239\"}}, 
               {\"runa-data\":{\"unit-price-dollars\":110.5,
                                             \"merchant-discount-dollars\":10.0,
                                             \"runa-discount-dollars\":20.2,
                                             \"session-type\":\"control\",
                                             \"order-id\": \"72144305\"},
                              \"merchant-data\":{\"unit-price-dollars\":110.0,
                                                 \"merchant-discount-dollars\":10.2,
                                                 \"runa-discount-dollars\":20.5,
                                                 \"session-type\":\"control\",
                                                 \"order-id\": \"72144305\"}},
              {\"runa-data\":{\"unit-price-dollars\":200.0,
                                             \"merchant-discount-dollars\":0.0,
                                             \"runa-discount-dollars\":30.0,
                                             \"session-type\":\"test\",
                                             \"order-id\": \"72144777\"},
                              \"merchant-data\":{\"unit-price-dollars\":200.0,
                                                 \"merchant-discount-dollars\":0.0,
                                                 \"runa-discount-dollars\":30.0,
                                                 \"session-type\":\"test\",
                                                 \"order-id\": \"72144777\"}}]}}"))

(def expected-json-headers {"Content-Type" "application/json; charset=utf-8"})

(deftest app-routes-test
  (testing "WHEN I GET /runatic/report?order-by routes with JSON content-type, "
    (is (= 200 (:status response-by-session-type)))
    (is (= expected-json-headers (:headers response-by-session-type)))
    
    (is (= 200 (:status response-by-order-id)))
    (is (= expected-json-headers (:headers response-by-order-id)))
    
    (is (= 200 (:status response-by-unit-price-dollars)))
    (is (= expected-json-headers (:headers response-by-unit-price-dollars))))
    
    (is (= expected-body-response-by-session-type
           (json/read-str (:body response-by-session-type))))
    
    (is (= expected-body-response-by-order-id
           (json/read-str (:body response-by-order-id))))

    (is (= expected-body-response-by-unit-price-dollars
           (json/read-str (:body response-by-unit-price-dollars)))))


