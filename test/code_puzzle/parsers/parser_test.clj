(ns code-puzzle.parsers.parser-test
  (:require [clojure.test :refer :all]
            [code-puzzle.parsers.core :refer :all]))

(deftest normalize-mapentry-test
  (testing "Converting to numbers"
    (is (= { "unit-dollars" 199.0} (normalize-mapentry ["unit-dollars" "199"])))
    (is (= { "unit-cents" 101.0} (normalize-mapentry ["unit-cents" "101"])))
    (is (= { "unit-id" "1"} (normalize-mapentry ["unit-id" "1"])))))

(deftest normalize-spaces-and-case-test
  (testing "normalizing headers to replace spaces with dashes and lowercase"
    (is (= (list "header-name1" "unit-price-dollars" "name-2")
           (normalize-spaces-and-case (list "Header Name1" "Unit Price Dollars" "name 2"))))))

(deftest split-on-test
  (testing "Unit testing split-on method"
    (is (= (map seq (list "this" "is" "splitted" "on" "spaces"))
           (split-on (seq "this is splitted on spaces") \space)))
    ))

